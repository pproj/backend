<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Transaction;

class FileController extends BaseController
{
    public function upload(Request $request) {
        if (!$data = $request->json()->get('data')) {
            return response(json_encode(['message' => 'No data']), 400);
        }

        $data = explode(',', $data);
        if (!is_array($data) || count($data) != 2) {
            return response(json_encode(['message' => 'Incorrect data']), 400);
        }

        if (!$data = base64_decode($data[1])) {
            return response(json_encode(['message' => 'Incorrect data']), 400);
        }

        if (!$transactions = $this->parseTable($data)) {
            return response(json_encode(['message' => 'Incorrect file report']), 400);
        }

        if (!$this->imports($transactions)) {
            return response(json_encode(['message' => 'Error imports data']), 400);
        }

        return response(json_encode(['message' => 'Ok']), 200);
    }

    private function imports(array $transactions): bool {
        app('db')->beginTransaction();

        foreach ($transactions as $transaction) {
            try {
                $exist = app('db')->select("SELECT 1 FROM transactions WHERE ticket = ? LIMIT 1", [$transaction->getTicket()]);

                if (count($exist)) {
                    continue;
                }

                app('db')->insert('insert into transactions
                        (ticket, open_time, type, comment, profit)
                    values
                        (?, ?, ?, ?, ?)',
                    [
                        $transaction->getTicket(),
                        $transaction->getOpenTime(),
                        $transaction->getType(),
                        $transaction->getComment(),
                        $transaction->getProfit()
                    ]
                );
            } catch (Exception $e) {
                app('db')->rollBack();
                return false;
            }
        }

        app('db')->commit();
        return true;
    }

    private function parseTable(string $html) {
        $transactions = [];

        $dom = new \domDocument;

        if (!@$dom->loadHTML($html)) {
            return false;
        }

        $dom->preserveWhiteSpace = false;
        if (!$table = $dom->getElementsByTagName('table')) {
            return false;
        }

        if (!$table->length) {
            return false;
        }

        $rows = $table->item(0)->getElementsByTagName('tr');

        if (!$rows->length) {
            return false;
        }

        $result = [];
        foreach ($rows as $row) {
            $cols = $row->getElementsByTagName('td');

            if ($cols->length < 5 || $cols[2]->nodeValue != 'balance') {
                continue;
            }

            $transaction = new Transaction();
            $transaction->setTicket($cols[0]->nodeValue);
            $transaction->setOpenTime($cols[1]->nodeValue);
            $transaction->setType($cols[2]->nodeValue);
            $transaction->setComment($cols[3]->nodeValue);
            $transaction->setProfit($cols[4]->nodeValue);
            $transactions[] = $transaction;
        }

        return $transactions;
    }
}
