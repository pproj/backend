<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Http\Request;

class OptionsController extends BaseController
{
    public function options(Request $request) {
        if ($request->isMethod('OPTIONS'))
        {
            return response('Ok', 200);
        }
    }
}
