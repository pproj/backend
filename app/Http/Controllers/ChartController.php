<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class ChartController extends BaseController
{
    public function get() {
        $result = app('db')->select("SELECT * FROM transactions ORDER BY open_time ASC");

        return json_encode($result);
    }
}
