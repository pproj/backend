<?php

namespace App;

class Transaction
{
    private $ticket;
    private $open_time;
    private $type;
    private $comment;
    private $profit;

    private function getTimeStamp(string $date): float {
        $dt = \DateTime::createFromFormat('Y.m.d H:i:s', trim($date));

        if (!$dt) {
            return 0;
        }

        return strtotime($dt->format('Y-m-d H:i:s')) * 1000;
    }

    public function setTicket(int $ticket): void {
        $this->ticket = $ticket;
    }

    public function getTicket(): int {
        return $this->ticket;
    }

    public function setOpenTime(string $open_time): bool {
        if (!$timestamp = $this->getTimeStamp($open_time)) {
            $this->open_time = 0;

            return false;
        }

        $this->open_time = $timestamp;

        return true;
    }

    public function getOpenTime(): float {
        return $this->open_time;
    }


    public function setType(string $type): void {
        $this->type = $type;
    }

    public function getType(): string {
        return $this->type;
    }

    public function setComment(string $comment): void {
        $this->comment = $comment;
    }

    public function getComment(): string {
        return $this->comment;
    }

    public function setProfit(string $profit): void {
        $this->profit = money_format('%i', str_replace(' ', '', $profit));
    }

    public function getProfit(): float {
        return $this->profit;
    }
}
